A realm is a bounded domain space,that is bounded by either other realms/domain walls/abyss(inter-domain space).

The best analogue for realms is servers controlling areas in multiplayer games, like a small virtual country. 

A realm size and shape is proportional to energy field of its generating agent(in higher realms the conglomerate of souls, egregore, god or Buddha-level entity controlling the realm). 

Earth(Domain of Earth) on physical plane is subdivided into huge country-size realms(focused out of Egregores corresponding to countries/provinces/islands:not 1:1 mapped to political entities as Egregores change control),
while astral/etheric Earths are subdivided into realms
of roughly city-size territory.
Mental plane realms are more flexible form, similar to
 houses or multidimensional mansions spread from a focus point(a Buddha-field is a large version that can span an entire domain).



















