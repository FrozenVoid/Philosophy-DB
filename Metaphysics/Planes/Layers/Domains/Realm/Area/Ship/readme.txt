A ship(spaceship, sea-ship) is a mobile house that is not anchored to specific egregore of the realm.



A typical boat anchored in a port forms a House in astral, but not when at sea.
A submarine cannot form a House.
A spaceship, ocean vessel, submarine are only bound to planetary egregore of the domain, with deep space spaceships essentially forming their own realm/egregore within the astral.