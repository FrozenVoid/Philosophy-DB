The "hungry mob argument" is a umbrella term for these economics-first policies, usually in third-world countries that prioritizes economic interest of the poor at expense of environment.
The argument, usually presented as care for the welfare of the people and builds on primitive understanding of ecology, states that "people need these resources now or they starve", justifying short-term exploitation of limited resources like forest for timber/pasture/agricultural fields.


The cultural subtext in these situations is not centered on environment or long-term welfare of the people, its a presentation of economic doctrine that sacrifices some future benefits(that people cannot perceive) for present time benefits(that people can easily see), which in culture lacking long-term impact awareness(e.g. presentism) appears far more convincing and appealing: present an easy solution to the hungry mob and it will follow.


Economic incentives for exploiting limited and irreplaceable resources(e.g. old-growth forests) with popular support combine to create strong reaction to environmental policies enacted from the outsider viewpoints(as disconnected from the needs of the people,"
the hungry mob").

The impact of environmental degradation(as in e.g. Haiti) is only perceived via long-term comparison, which lacks focus and is normalized by gradual change, which the "hungry mob" does not view as something "out of ordinary" or a catastrophe  - until the conditions(soil erosion, crop failure, drought) begin to manifest; even at this point the situation is viewed as normal event disconnected from the degradation of the exploited environment.

