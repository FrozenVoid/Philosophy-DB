MCS is umbrella term for modern narcissist inclination of people raised on fictional works where ego-centric characters dominate:
superhero fiction, heroic novels, etc lead many to construct a role model of Main Character that demands the plot attention. The impression of own "heroic nature" is ingrained due
fictional/imagined celebrity role model that MCS follows, usually
explained as "being the best version of yourself"(MCS takes this to extreme superheroic role, disputing any flaw/failure).
Earlier predecessors for this condition are heroic complexes
and delusions of grandeur, MCS being adapted to impersonal view of digital society(camera phones, instant attention, flash mobs,viral video,etc) where people gain quick fame in
any incident to catapult ordinary "Character" into limelight of online crowds.