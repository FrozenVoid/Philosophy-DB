

https://en.wikipedia.org/wiki/Physicalism
Physicalism is broad extension of materialist reductionism toward "physical monism": proclaiming that all is composed of physical features: compared to classic materialism  it allows space for fields, quantum randomness and non-material emergentism.

Objections to pure material world of raw particles come after discoveries in quantum
physics: the world of billiard ball mechanics and neat models of "matter at motion" gave way to more complex interactions where force-fields and subtle 
quantum phenomena on micro-scale were put
in the limelight of science, forcing philosophers of 
materialistic doctrines to accommodate or reject them
as "immaterial". Physicalism won as dominant philosophy
of science and remains as powerful basis of materialistic
reductionism, despite rejecting prior assumptions on
reality of matter and energy(classical mechanics) with
science advancing towards understanding of invisible 
force-fields and subatomic particles.

Emergentism: Emergence of consciousness and complex non-physical phenomenons in physicalism is viewed as compromise between
views of "physical materialism"(reducing unknown phenomena towards their material basis in known physics) and "metaphysical materialism" where substance of non-physical phenomena is
classified as unknown field/particle acting on physical world(similar to Dark Matter/Dark Energy explanation in astrophysics).




