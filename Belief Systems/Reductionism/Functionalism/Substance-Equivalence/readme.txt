Substance equivalence is one of more disconcerting consequences of functionalism.
The role of substance and its external appearance, are mere 
categories to be operated on, rather than concrete 
objects with similarity: the functional role of object is
the category. Uniqueness and inherent subjective perceptions(qualia)
are denied epistemological validity: functionalism elevates the category
of object to its "functional form" without distinction.

Reduction for substance within simulation:
https://en.wikipedia.org/wiki/Functionalism_(philosophy_of_mind)

A virtual construction of an object
operating in a virtual world that supplies all
physical qualities of equivalent object is treated as same as original, despite being different in substance(e.g. virtual cat vs physical cat). 
A being living inside a virtual world would treat
the virtual object with same qualities as equivalent.