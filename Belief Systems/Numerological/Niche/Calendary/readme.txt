Numerological interpretation of  dates/times of events, using specific calendar system: the assigned meaning is combination of digits or their sum.

This system is often used to construct arbitrary numbers or relation due limits of calendary combinations by crank numerologists: the range of possible combinations and sums is much smaller than gematria.