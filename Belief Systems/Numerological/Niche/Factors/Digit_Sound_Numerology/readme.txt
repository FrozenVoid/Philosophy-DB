Digit names in local languages are interpeted as elemental combination of letters, with letters of each digits summing to dominant and inferior elements:
e.g. one =<earth> <air><air>
 
The system derives meaning from phonetic energy of numbers.