The Prime Factor system assigns meaning to number above 9, the meaning of their prime factor:
e.g. 12=2*2*3, with 2 being more significant.

Prime number are calculated using the traditional digit sum(4=1+3=13) or treated as combination of digits(1,3)