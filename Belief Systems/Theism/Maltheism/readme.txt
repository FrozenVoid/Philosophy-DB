Maltheism is theistic doctrine that depicts 
a universe ruled by an evil god.

As solution to Problem of Evil: It rejects the
inherent benevolence/goodness of  world-creating entities
and therefore no paradox of omnipotence/omniscience 
is neccessary to solve. A multi-level spiritual
hierarchy that allows such a God to exist and prosper
is by contagion complicit, so even Gnostic belief are fully
 maltheist.

This position is compatible with all major
religions as they admit God(s) being "Beyond Good and Evil",
so in their moral character are not good(even when
demanding perfect goodness themselves).

A Simulationist view is also compatible as creators
of a simulation aren't required to be more moral than
its content. 

Multi-level delegation of evil:
Buddhist views on samsaric cosmology being ruled by
Brahma-type entities within and
 Gnostic doctrines of limited Demiurge
 trying to shape reality into its own image, are
 also consistent with Maltheism.
 
Rotten Foundation Problem:
 The problem of Maltheism only begins to 
reveal itself as foundation of morality in religion
is outsourced to Gods law or assumed "higher bureaucracy"
that is standing on moral high ground and defining
what is "moral" (instead of intuitive/emotional judgement).

