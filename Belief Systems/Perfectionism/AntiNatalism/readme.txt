Anti-Natalism is an ethical doctrine that
following the idea of minimizing future suffering, by not procreating or enabling pro-creation as a causative force:

A form of perfect state, the species which follows anti-natalism will become extinct,
thus earning perfect comfort.

Perfectionism: the anti-Natalist doctrine is
enforcing a very high ethical perfection, which accepts only one position that is 
ethical logically:

A.Procreation and enablement thereof, create 
suffering/bad karma/future evil.

B.Not procreating or being a cause of procreation,  remove the possibilities of suffering/bad karma/evil from existence.


i.e. the logical conclusion for maximizing ethical contribution would be B. but such
position is essentially demanding a perfect
score: with most processes(evolution/society/religion) optimized towards Natalism, reducing the perfect ethical score.





