Elevators are a modern example of fully enclosed liminal space. Unlike a room, the elevator shifts ground ether-material level from 
"true ground" towards the floor, allowing vertical movement through ether-material plane.

Elevators are very isolating, like a "terrestrial ship" sailing through space, this is enchanced by complete disconnection of  environment,unlike ships which have observation capabilities, disorienting movement that is unlike transportation, with a uncertainty of destination, creating expectations of arriving somewhere.


These expectation-focusing and isolation provide a powerful platform for many occult and esoteric practices with elevators like the "Elevator Game" that utilize the uncertainty/expectation energy flow of human passengers to shift the destination from its usual ending point in the planar scheme(*shifting the planes by etheric dissociation in elevator).


From the perspective of safety the elevators are equivalent to planes and ships, a nexus of transportable entities using it as etheric hub or for attack on passengers.

The etheric     equivalent (the mirror elevator moving in etheric plane) represents the most malleable "enclosed space" unconnected to any "grounding terrain"(unearthed platform),thus making it intra-planar "island" separated from the egregore of building.
(who is grounded to Earth)