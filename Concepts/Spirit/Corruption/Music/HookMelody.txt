Hook melody is a short(typically less than 10s) instrumental portion that has 
pleasurable effects in perception of listener. This is the main draw(bait) that
induces interest in the song content.


The hook composition is designed to appeal
to some hidden energetic need, providing
a progression of frequencies that are synergized to draw attention, focus and activate
specific chakra(chakra opening),
allowing a specific mental response that
marks following melody/lyrics as emotionally sensitive/important, despite these parts in isolation not
evoking same response.

Hooks operate by inducing etheric and astral flows,
like a key to activate memory recall and associative-passive
state where song is absorbed without critical thinking:
the mental portion of a hook serves as
disabling context in a mental states(off switch),
relying on emotional response activation to shift
from thinking patterns(thought-stopper) that treat
lyrics/instruments as "neutral data"(analytic) into
a state of "passive reception"(emotional alignment),
where lyrics/instruments are treated as composed of
emotional states that "sync" with the mind of listener:
however this is an illusion brought by the 
hook melody opening this state and parts of song
in isolation are poorly absorbed as "emotional states"
the rhythm and emotional-sequence progression has to be ingrained
by repetition(refrain, replay,recall) in memory.
 
 
Heart chakra - hooks always operate on heart chakra energy state,
even if the hook melody doesn't open its channells(common
in harsh music genres, where sacral chakra's raw sense emotions
 dominate),
as emotional alignment is required for enjoyment
of song content: without the "hook melody"(or alternatively
a specific "vocal hook"(see HiddenMantra.txt))
the emotional judgement of song is much lower.


Benefits of hooks: Instrumental melodies can
in some cases be beneficial
to emotional balance or for unblocking heart chakra
channells due powerful activation process triggered by hooks.
 The benefits don't extend to rest of song/melody of course.
