Ichor is the etheric body liquid that allows the physical body to live(vital power).
It is produced by manipura chakra, from amrita(throat nectar) and other substances such as food or specific etheric energy(crown chakra substances) consumed/directed
to its center. 

Higher planes vampires/demons can drain ichor via direct cords or specific astral implants, leaving the human in a state of weakness/lethargy. Ichor amplifies their vital energies and magical powers as their etheric bodies are  atrophied from being incarnated 
in a plane higher than their constitution allows(i.e.
keeping demonic entities in hi-freq state).
Ichor farming(etheric) and loosh farming(astral) are 
performed by competing entities who anchor on different planes.