A bound portal is a portal that is projected into an item,
such as a door, crystal, ring and is limited by its material-etheric structure.

Usually the portal is opened when the item is stationary and/or secured to some other "ground item"(providing stability).


Types:
A.Inner:portal opens inside an item, like a palantir, and its outer exit moves from  energy flow directed towards the edges of portal from the owner side. Material items cannot be transfered, but etheric/astral entities can cross.

2.Outer: a door-like portal bound within a gate/door, that open a wormhole-like corridor into another location, allowing material items to cross, the edges of portal are bound to bounding-object edges and the exit window is usually a similar object(e.g. door A to Door B).


3.Loose: an object like an altar, allows opening the portal and manipulating it in a free-form within some radius of itself.
 A more advanced form a ring, allowing something like directing a opening from fixed portal elsewhere, or opening portals bound to a place by activation of a ring(usually by spinning it clockwise/counter-clockwise to de/activate local portal node). 

