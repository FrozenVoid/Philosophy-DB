Binaural beats are mixed with source audio in an effort to modify brain chemistry and
associative memory, with various physiological effects.

Types:
1.Inserted by uploader:
Usually without disclosure or with the guise of ASMR/meditation music/etc.

2.Inserted by website operator:
Usually mixed within downloaded audio files or added to livestream. These can be targeted more narrowly.

3.Inserted via man-in-the-middle attack:
CDN, proxy or transit servers modify the files downloaded. Much rarer but possible with compromised websites.






Some common inserts:
1.Brown notes: low Theta-wave content that increase excretion, feeling of disgust(associative memory) and (as with all Theta wave) faciliate hypnotic suggestion.


2.Hypnotic trance:
Usually a mix of delta/theta beats, faciliates unguarded absorbtion of content.



3.Rejection tones: Used to alter associative memory by raising Vata humor(aesthethic sense) during view of controversial content the website operator wants to associate with negative emotion.


4.Friendly  tones: beats that use grounding
frequencies(Earth element) which provoke feelings of unity/brotherhood/inter-connectedness etc to associate with 
content promoted by website/file creator.



5.Overload beats: Gamma wave combinations that intentionally cause fast chains of thoughts and rapid, stressful thinking patterns. Used as negative reinforcement of negative content to increase its power of suggestion(e.g. Fear porn).



6.Focusing tones: Beta/Alpha wave mix.
Turning the consumer into relaxed but focused mode to absorb content into long-term memory. Used for "correcting the record", overwriting memory and forming false narratives.



















