Non-Player Character or NPC is a RPG term that evolved from referring to in-game entities(neutrally) towards a pejorative 
figure that is "limited by design".
Usually the NPC in its non-gaming meaning refers to a person that lacks critical thinking skills and acts by rigid role that looks like a NPC dialogue script:
the limited number of dialogue choices or inherent low complexity of dialogue trees in RPGs are adapted to the idea of one 
mind being overtaken by a "dialogue script" that allows no deviation or critical self-assesment, instead operating like a 
cult follower of specific "NPC script".

Rise in popularity: The "NPC script" interaction were attributed to "shallow political fandoms" of late 2010's where right-wing and left-wing polarization allowed to delineate some "NPC character traits" that resembles inflexible dialogue trees akin to artificial algorithm sorting 
input automatically("bad thought/good thought"). The NPC script idea was escalated with discovery of lack of inner monologue/dialogue where perceived "NPCness"  was assigned to people who lack internal verbalization of mindstream:
this perception has shaped the NPC label to a dehumanizing level that judges personalities by their mental complexity.

The stereotype of a NPC evolved from RPG roots towards a primitive Artificial Intelligence, with various lifehacks/tricks/ideas towards "breaking the script" or forcing the NPC to engage with some critical thoughts or even introspect his own value/judgement away from reflexive dismissal.



