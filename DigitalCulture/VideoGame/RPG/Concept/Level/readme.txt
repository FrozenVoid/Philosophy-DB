Levels are numerical classes that allow
a game to separate "character progression"  into chunks, distanced by effort.
The paradigm of "level grinding" follows from the idea that "hard work achieves anything",i.e. mechanically repeated task will magically increase one's skill, level or class towards some final "peak level" in that domain,without
are regression or negative side-effects from "grinding away".

This perception of easy-achieved mastery or
mechanical progression with time or concentration of effort, has created a psychological justification(similar to "Work ethic") towards investing continious energy and resources towards unachievable perfect state/goal that is justified as 
:getting closer to next level", "progression towards perfection", while ignoring the limited amount of progress achieve with practice and limited lifespan being insufficient to achieve such levels by  brute-force grinding(Hard Work paradigm).

In reality the change in levels comes from change in class/skill/configuration of the mind(quality investment), not from mechanistic work towards a goal(quantity investment),
which could by coincidence allow some insight/improvement but not on the level of focused qualitative change.
The repetitive routine is actually a dehumanizing, robotic
 ritual that reduces perceptual progress in favor of 
 following "expirience gain". An exceptional script may
 gain some benefit for following it(work-> reward), but such script allows
  only fixed reward per effort, not level advancement.



