An object in the energetic center of a room or enclosed space, usually at geometric center. The energies of object elements usually pervade the room energy flow:
the east-west/north-south flows intersect at object location, which creates special focusing vortexes where local elemental flows are supplanted by the object energy.


This is the principle of altars, temples and churches:
their energy centers
 correspond to geometric center of ground at same level(raised podiumspodiums shift level of ether-material plane to isolate the energy into a "virtual room" easier to center,like an altar circle).



Geometric center can be shifted by manipulating and blocking magnetic flows of 4 corners, to create a different central position, though it will alter the balance of room elements(usually used to block grounding(Earth-West) or increase Fire-East).