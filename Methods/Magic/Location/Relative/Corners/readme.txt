The corner is a place where walls intersect:

A.Interior corner: The corner isolates the influence of elements coming from specific magnetic field direction,
e.g. Eastern corner is Fire element,Western Eart,Northern Air,Southern Water.


E.Exterior corner: the walls block the element of the prominence, e.g. Eastern edge absorbs Fire, creating a space where element is depleted for  some distance from exterior edge. however the effect of the corner largely depends on objects in the  interior corner(e.g. Southern edge depends on interior South corner), which can block or intensify elemental flow coming through it.






