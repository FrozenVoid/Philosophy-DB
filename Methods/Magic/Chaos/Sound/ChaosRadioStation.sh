alias rndhex="wget  https://qrng.anu.edu.au/wp-content/plugins/colours-plugin/get_block_alpha.php -q -O -"

#play N random texts from QRNG  via espeak
#bash example: rndplay 100

#increase sleep modulo for bigger gaps between texts
function rndplay(){
for ((i=1; i<=$1; i++)); do
  rndhex |espeak -a 90
  sleep $((RANDOM % 10))
done
}