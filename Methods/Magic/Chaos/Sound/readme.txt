Random sounds, noise can affect
mental patterns and change the mood entirely.

Some interesting methods:

1.Disrupting a mental loop:
the idea is that verbalizationg of any thought can be countered by forcing interpretation of competing verbalization/language-source.


a random text, like a numbers station will break the focus on a mental loop and even provide
  inspiration to creative thoughts.



  2.Disrupting magical spells:
  Any spell/mantra can be altered by sound patterns, with chaotic noises erodings the flow of energy in it.


3.Break mental focus of listening entities: 
The presence of non-interpretable sounds(like random text),
create a deterrent for focusing energy, so any entity focusing
in the vicinity has to refocus: this can repel some entities from
listening/focusing on the area.


see ChaosRadioStation.sh for example of chaotic text verbalization.