Thge idea is instead of using rnadom numbers to influence 
the path, the choice is driven
by specific events pattern.

e.g. If it rains today, do A, else do B.

as replacement for "if the first number that appears is odd, do  A else do B" 



The idea here is that the 
event("it will rain today") will be linked to action(insured), that would make the entity responsible for rain also responsible(originating condition) for the taken action, which for most entities with omniscience/future prediction will shift the timeline to insure that only the right "action A/B" is allowed, with more linked(insured) actions dependent on event creating a dependency chain trigger that makes the event itself more timeline-altering: e.g.

make 20 choices(A or B) randomly dependent on whatever it rains today, and timeline is "entropy insured" to follow the path that aligns with best "action set" as viewed by entities originating the events
