The heart-crown channel suffix,
analogue to -NG.
Powerful water-air axis energies are unlocked with this suffix.