The boundaries of palm center(Void element) have confluence points where 
multiple elements merge.

1.Kapha confluence: below the fingergap of pinky/ring

2.Vata confluence: below the fingergap of index/middle.


3.Apana merge: below middle/ring fingergap.

4.Rudra merge: in the gap between index 

 and thumb, closer to center.


 These confluence points can be felt when elemental flow of  specific stream is activated(hot energy feeling) or drained(felt like pain).


 These confluence points are linked to the palm center and its mainline energy ether-void axis supplies(space configuration of elements)


