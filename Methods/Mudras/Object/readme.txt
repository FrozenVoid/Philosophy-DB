Object mudras usually utilize
 an object held between fingers to transform/filter energy flow.
 E.g. hold a blue button between middle and thumb finger to boost etheric flow(color blue) or an piece of iron to boost fire.