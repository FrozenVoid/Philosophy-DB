Positive prompts:

Reference: 
cat : https://ibb.co/ZKgBQKv
Depicts stereotypical "evil" cat character with angry/feral expression.
cat,(evil:1):  https://ibb.co/18jdHnC
cat,(evil:1),(evil:1),: https://ibb.co/k0JcZLV
cat,(evil:1),(evil:1),(evil:1),:https://ibb.co/mG3rHPW

negative: Effect of increasing evil
cat,(evil:-1): https://ibb.co/QF2snCS
cat,(evil:-1),(evil:-1),(evil:-1):  https://ibb.co/vLWSLW7

Fused: 
cat (evil:1)(evil:1)(evil:1) : https://ibb.co/K58p3x4
cat (evil:-1)(evil:-1)(evil:-1): https://ibb.co/hfPGgPC

 Fused Null Evil:cat (evil:0)(evil:0)(evil:0)(evil:0)
 : https://ibb.co/2yX5RfK
 Separate Null Evil: cat ,(evil:0),(evil:0),(evil:0),(evil:0),: https://ibb.co/6RGJHqq
 
 
 

Effects on negative prompt(positive "cat"):
Makes the subject look more innocent and graceful. Increased light and positive emotional presence.
 (evil:1):https://ibb.co/V9tG7BV
  (evil:1)(evil:1)(evil:1) : https://ibb.co/Y21gC5h
    (evil:1),(evil:1),(evil:1) : https://ibb.co/PGrL1vj
a longer negprompt results in image closer to reference cat with lighter fur and more positive expression:
    (evil:1),(evil:1),(evil:1),  (evil:1),(evil:1),(evil:1),  (evil:1),(evil:1),(evil:1),  (evil:1),(evil:1),(evil:1),  (evil:1),(evil:1),(evil:1),  (evil:1),(evil:1),(evil:1),  (evil:1),(evil:1),(evil:1),  (evil:1),(evil:1),(evil:1),  (evil:1),(evil:1),(evil:1),  (evil:1),(evil:1),(evil:1),  (evil:1),(evil:1),(evil:1),  (evil:1),(evil:1),(evil:1),  (evil:1),(evil:1),(evil:1),  (evil:1),(evil:1),(evil:1),  (evil:1),(evil:1),(evil:1),  (evil:1),(evil:1),(evil:1),  (evil:1),(evil:1),(evil:1),  (evil:1),(evil:1),(evil:1),  (evil:1),(evil:1),(evil:1),  (evil:1),(evil:1),(evil:1),  (evil:1),(evil:1),(evil:1),  (evil:1),(evil:1),(evil:1),  (evil:1),(evil:1),(evil:1), :  https://ibb.co/hLG4JsG

Fused:
(evil:1)(evil:1)(evil:1)  (evil:1)(evil:1)(evil:1)  (evil:1)(evil:1)(evil:1)  (evil:1)(evil:1)(evil:1)  (evil:1)(evil:1)(evil:1)  (evil:1)(evil:1)(evil:1)  (evil:1)(evil:1)(evil:1)  (evil:1)(evil:1)(evil:1)  (evil:1)(evil:1)(evil:1)  (evil:1)(evil:1)(evil:1)  (evil:1)(evil:1)(evil:1)  (evil:1)(evil:1)(evil:1)  (evil:1)(evil:1)(evil:1)  (evil:1)(evil:1)(evil:1)  (evil:1)(evil:1)(evil:1)  (evil:1)(evil:1)(evil:1)  (evil:1)(evil:1)(evil:1)  (evil:1)(evil:1)(evil:1)  (evil:1)(evil:1)(evil:1)  (evil:1)(evil:1)(evil:1)  (evil:1)(evil:1)(evil:1)  (evil:1)(evil:1)(evil:1)  (evil:1)(evil:1)(evil:1) : https://ibb.co/hfc1BZz



If negative values/prompts are given(e.g. (evil:-4) ) the effect is  incresing 
maturity and grittiness, negative facial expressions and overall gloomy mood. The subject with "negative evil" becomes more fake, deceiving shapes and colors appear, offering a feeling of treachearous spirit inside.


 (evil:-1)(evil:-1)(evil:-1): https://ibb.co/9tqdShx
  (evil:-1),(evil:-1),(evil:-1): https://ibb.co/PZRpTfK
  
  
