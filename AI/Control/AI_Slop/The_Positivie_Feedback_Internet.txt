

The fundamental disconnect from honest feedback:
Its the lack of negative feedback, when you have social media and search engines optimized for positive feedback.

A typical social media website(except e.g. Reddit and its clones) doesn't "Downvote" content, so bad content(automated spam) can ovewhelm the rest easily(bots have no fatigue or limits to push against) - and there are huge economic incentives to push that content, and there is no option to truly rate content on a scale(e.g. percent/score) so mediocre content with enough likes/upvotes/etc dominates due staying in the feed long enough.

Search Engines failure to rate content:
A search engine typically provide a one-way "report button" but zero options to rate quality or alignment with the query content(e.g."superficially correct answers" with zero substance), processed by AI at best and with typical "word-crunching" scripts at worst that just assume content is relevant based on titles and brief keyword checks.

The anti-AI crowd blaming AI and spammers, don't realize the system itself incevitizes the lowest-common-denominator content to be pushed to the top, number of likes/links/+1 ratings don't
work. The point of this "content wave" doesn't depend on "being correct" but "being profitable" so the spammers will push as much content as they want, as long their goals are met they don't stop - their accounts don't get downranked or their overall rating never goes down(the social media "only positive +1 feedback") only grows with every upload.
A system where account reputation suffers badle from submitting content that is downranked is the only one that will work in principle, and of course creating new accounts to avoid negative feedback has to be carefully implemented:
if the system is too harsh it might alienate normal users or force them to abandon downranked accounts or being falsely labeled as "bot-like","AI-generated"(by automated content checking software).



A robust rating system and anti-vote-manipulation measures are required.
All user-contributed content on the internet needs to switch from positive-feedback systems before they become 99.99% AI slop, from search engines to social media, low-effort content should stop being treated as "normal state" and instead harshly downranked/downvoted/downrated by the widest amount of users, instead of accepting "category:mismatch" type reports or "fake content" reports(that very few users bother with, the ratings has to be zero-hassle system to attract more human feedback).

The problem: platforms want to provide "zero negative" feelings for users submitting content in assumption that all new content expands their data "clout" on the internet. This Assumption is deadly WRONG: internet can be filled with zero-cost AI-generated drivel by the truckload,
since most incentives to avoid posting "bad content" were removed for "reducing user estangement and comfort"(social media "enculturation within a group of peers"),
this 'no hard feelings, no honest feedback, no negative signals' atmosphere created the perfect 
feedstock for growing AI bot-spam,

How bad is this going to get:
1.Search engines: lack of ratings will force migration to "directoru/aggregator" websites.
2.Social media; without ratings, the feeds and 
content channels of such web portals will become 
100% AI and economic incentives will make 
AI spammers emulate the best-performing content.

Blaming AI as a tool will not work:
There is no point restricting AI itself or
public providers, open-source AI content generation already provides content of same quality level, so the legal actions and misguided attempts to limit public websites will just hurt regular users. Any AI spammer can afford to
invest in bots/servers/GPUs or various cloud services(e.g. AWS) will move to generating in-house content,e.g. with open source Image/text generators that run on modern GPUs. 